<?php


namespace Jonas\Databases\Exceptions;


/**
 * Class NoResultException
 * @package Jonas\Databases\Exceptions
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class NoResultException extends DatabaseException
{
    public function __construct()
    {
        parent::__construct('No results found');
    }
}
