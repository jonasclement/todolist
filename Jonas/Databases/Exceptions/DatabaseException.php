<?php


namespace Jonas\Databases\Exceptions;


use RuntimeException;
use Throwable;

/**
 * Class DatabaseException
 * @package Jonas\Databases\Exceptions
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class DatabaseException extends RuntimeException
{
    /**
     * DatabaseException constructor.
     * @param string $message
     * @param int $code Use non-zero to indicate an ERROR
     * @param Throwable|null $previous
     */
    public function __construct(
            string $message = "An error occurred while performing the database action",
            int $code = 0,
            ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
