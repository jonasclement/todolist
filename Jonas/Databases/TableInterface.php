<?php


namespace Jonas\Databases;


/**
 * Interface TableInterface
 * @package Jonas\Databases
 * @author Jonas Clément <contact@jonasclement.dk>
 */
interface TableInterface
{
    public function __construct();
}
