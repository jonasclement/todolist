<?php


namespace Jonas\Databases;

use mysqli;

/**
 * Class Database
 * @package Jonas\Assignment\Database
 * @author Jonas Clément <contact@jonasclement.dk>
 */
abstract class Database
{
    // $DB_CONN is static to avoid opening a new connection every time we instantiate a table class
    protected static mysqli $DB_CONN;

    protected string $databaseName;
    protected string $tableName;

    protected function __construct(string $host, string $user, string $password, string $database, $port = 3306)
    {
        mysqli_report(MYSQLI_REPORT_STRICT);
        if (!isset(self::$DB_CONN) || !self::$DB_CONN->ping()) {
            self::$DB_CONN = new mysqli($host, $user, $password, $database, $port);
        }
    }
}
