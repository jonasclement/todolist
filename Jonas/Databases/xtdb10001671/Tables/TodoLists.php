<?php


namespace Jonas\Databases\xtdb10001671\Tables;


use DateTime;
use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\TableInterface;
use Jonas\Databases\xtdb10001671\Models\TodoList;
use Jonas\Databases\xtdb10001671\Models\User;
use Jonas\Databases\xtdb10001671\xtdb10001671_db;

/**
 * Class todoLists
 * @package Jonas\Databases\xtdb10001671\Tables
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class TodoLists extends xtdb10001671_db implements TableInterface
{
    public function __construct()
    {
        parent::__construct('todo_lists');
    }

    /**
     * Get all to-do lists with their items by user
     * @param User $user
     * @return TodoList[]
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function getAllListsWithItemsForWebByUser(User $user): array
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, name, color FROM `{$this->tableName}` WHERE ownerUserID = ?");
        if ($statement) {
            $statement->bind_param('i', $user->id);
            $statement->execute();
            $statement->bind_result($id, $name, $color);

            $dbTodoListItems = new TodoListItems();
            $returnLists = [];
            while ($statement->fetch()) {
                $items = $dbTodoListItems->getItemsByTodoListID($id);
                $returnLists[] = new TodoList($id, $user, $name, $color, null, $items);
            }
            $statement->close();
            return $returnLists;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    /**
     * Create new list
     * @param TodoList $list
     * @return TodoList The given list, updated with ID and approximate create time
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function create(TodoList $list): TodoList
    {
        $statement = parent::$DB_CONN->prepare("INSERT INTO `{$this->tableName}` (ownerUserID, name, color) VALUES (?, ?, ?)");
        if ($statement) {
            $statement->bind_param('iss', $list->ownerUser->id, $list->name, $list->color);
            $statement->execute();
            if ($statement->error) {
                $statement->close();
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();
            $list->id = parent::$DB_CONN->insert_id;
            $list->createTime = new DateTime();

            // Create items
            $dbTodoListItems = new TodoListItems();
            foreach ($list->items as $item) {
                $dbTodoListItems->create($item);
            }

            return $list;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function update(TodoList $list): bool
    {
        $statement = parent::$DB_CONN->prepare("UPDATE `{$this->tableName}` SET ownerUserID = ?, name = ?, color = ? WHERE id = ?");
        if ($statement) {
            $statement->bind_param('issi', $list->ownerUser->id, $list->name, $list->color, $list->id);
            $statement->execute();
            if ($statement->error) {
                $statement->close();
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();
            return true;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function getListByID(int $id): TodoList
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, ownerUserID, name, color, createTime FROM `{$this->tableName}` WHERE id = ? LIMIT 1");
        if ($statement) {
            $statement->bind_param('i', $id);
            $statement->execute();
            $statement->bind_result($id, $ownerUserID, $name, $color, $createTime);
            $statement->fetch(); // only expecting one row so no need to loop

            if ($id === null) {
                throw new NoResultException();
            }

            // Get user
            $dbUsers = new Users();
            $user = $dbUsers->getUserByUserID($ownerUserID);
            // Get items
            $dbTodoListItems = new TodoListItems();
            $items = $dbTodoListItems->getItemsByTodoListID($id);
            return new TodoList($id, $user, $name, $color, new DateTime($createTime), $items);
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function deleteListByID(int $id): void
    {
        $statement = parent::$DB_CONN->prepare("DELETE FROM `{$this->tableName}` WHERE id = ?");
        if ($statement) {
            // Delete all items
            $dbTodoListItems = new TodoListItems();
            $dbTodoListItems->deleteItemsByTodoListID($id);

            $statement->bind_param('i', $id);
            $statement->execute();
            if ($statement->error) {
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }
}
