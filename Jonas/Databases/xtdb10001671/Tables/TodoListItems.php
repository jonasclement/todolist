<?php


namespace Jonas\Databases\xtdb10001671\Tables;


use DateTime;
use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\TableInterface;
use Jonas\Databases\xtdb10001671\Models\TodoListItem;
use Jonas\Databases\xtdb10001671\xtdb10001671_db;

/**
 * Class TodoListItems
 * @package Jonas\Databases\xtdb10001671\Tables
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class TodoListItems extends xtdb10001671_db implements TableInterface
{
    public function __construct()
    {
        parent::__construct('todo_list_items');
    }

    /**
     * Get list items for the given list
     * @param int $todoListID
     * @return TodoListItem[]
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function getItemsByTodoListID(int $todoListID): array
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, todoListID, text, done, createTime FROM `{$this->tableName}` WHERE todoListID = ?");
        if ($statement) {
            $statement->bind_param('i', $todoListID);
            $statement->execute();
            $statement->bind_result($id, $todoListID, $text, $done, $createTime);

            $returnItems = [];
            while ($statement->fetch()) {
                $returnItems[] = new TodoListItem($id, $todoListID, $text, $done, new DateTime($createTime));
            }
            return $returnItems;
        } else {
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    /**
     * Create a new list item
     * @param TodoListItem $item
     * @return TodoListItem Given item updated with ID and approx. create time
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function create(TodoListItem $item): TodoListItem
    {
        $statement = parent::$DB_CONN->prepare("INSERT INTO `{$this->tableName}` (todoListID, text, done) VALUES (?, ?, ?)");
        if ($statement) {
            $statement->bind_param('isi', $item->todoListID, $item->text, $item->done);
            $statement->execute();
            if ($statement->error) {
                $statement->close();
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();

            $item->id = parent::$DB_CONN->insert_id;
            $item->createTime = new DateTime();
            return $item;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function deleteItemByID(int $id): void
    {
        $statement = parent::$DB_CONN->prepare("DELETE FROM `{$this->tableName}` WHERE id = ?");
        if ($statement) {
            $statement->bind_param('i', $id);
            $statement->execute();
            if ($statement->error) {
                $statement->close();
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function updateItemByID(int $id, string $text, bool $done): void
    {
        $statement = parent::$DB_CONN->prepare("UPDATE `{$this->tableName}` SET text = ?, done = ? WHERE id = ?");
        if ($statement) {
            $statement->bind_param('sii', $text, $done, $id);
            $statement->execute();
            if ($statement->error) {
                $statement->close();
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function getById(int $id): TodoListItem
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, todoListID, text, done, createTime FROM `{$this->tableName}` WHERE id = ? LIMIT 1");
        if ($statement) {
            $statement->bind_param('i', $id);
            $statement->execute();
            $statement->bind_result($id, $todoListID, $text, $done, $createTime);
            $statement->fetch(); // only expecting one row so no need to loop

            if ($id === null) {
                throw new NoResultException();
            }

            return new TodoListItem($id, $todoListID, $text, $done, new DateTime($createTime));
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function deleteItemsByTodoListID(int $id): void
    {
        $statement = parent::$DB_CONN->prepare("DELETE FROM `{$this->tableName}` WHERE todoListID = ?");
        if ($statement) {
            $statement->bind_param('i', $id);
            $statement->execute();
            if ($statement->error) {
                $statement->close();
                throw new DatabaseException($statement->error, $statement->errno);
            }
            $statement->close();
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }
}
