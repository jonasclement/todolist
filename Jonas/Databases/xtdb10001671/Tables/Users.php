<?php


namespace Jonas\Databases\xtdb10001671\Tables;


use DateTime;
use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\TableInterface;
use Jonas\Databases\xtdb10001671\Models\User;
use Jonas\Databases\xtdb10001671\xtdb10001671_db;

/**
 * Class Users
 * @package Jonas\Databases\xtdb10001671\Tables
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class Users extends xtdb10001671_db implements TableInterface
{
    public function __construct()
    {
        parent::__construct('users');
    }

    public function getUserByUserID(int $userID): User
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, username, password, allowLogin, isAdmin, createTime FROM `{$this->tableName}` WHERE id = ? LIMIT 1");
        if ($statement) {
            $statement->bind_param('i', $userID);
            $statement->execute();
            $statement->bind_result($id, $username, $password, $allowLogin, $isAdmin, $createTime);
            $statement->fetch(); // only expecting one row, so no need to loop

            if ($id === null) {
                throw new NoResultException();
            }

            $user = new User($id, $username, $password, $allowLogin, $isAdmin, new DateTime($createTime));
            $statement->close();

            return $user;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    /**
     *
     * @param string $username
     * @return User
     * @throws NoResultException|DatabaseException|\Exception
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function getUserByUsername(string $username): User
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, username, password, allowLogin, isAdmin, createTime FROM `{$this->tableName}` WHERE username = ? LIMIT 1");
        if ($statement) {
            $statement->bind_param('s', $username);
            $statement->execute();
            $statement->bind_result($id, $username, $password, $allowLogin, $isAdmin, $createTime);
            $statement->fetch(); // only expecting one row, so no need to loop

            if ($id === null) {
                throw new NoResultException();
            }

            $user = new User($id, $username, $password, $allowLogin, $isAdmin, new DateTime($createTime));
            $statement->close();

            return $user;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    /**
     * Get all existing users
     * @param int $limit
     * @param int $offset
     * @return User[]
     * @throws DatabaseException|\Exception
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function getAllUsers(int $limit = PHP_INT_MAX, int $offset = 0): array
    {
        $statement = parent::$DB_CONN->prepare("SELECT id, username, allowLogin, isAdmin, createTime FROM `{$this->tableName}` LIMIT ? OFFSET ?");
        if ($statement) {
            $statement->bind_param('ii', $limit, $offset);
            $statement->execute();
            $statement->bind_result($id, $username, $allowLogin, $isAdmin, $createTime);

            $returnUsers = [];
            while ($statement->fetch()) {
                $returnUsers[] = new User($id, $username, '', $allowLogin, $isAdmin, new DateTime($createTime));
            }
            $statement->close();
            return $returnUsers;
        } else {
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    /**
     * Get the current amount of admin users
     * @return int
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function getAmountOfAdminUsers(): int
    {
        $statement = parent::$DB_CONN->prepare("SELECT COUNT(*) as amount FROM `{$this->tableName}` WHERE isAdmin = 1");
        if ($statement) {
            $statement->execute();
            $statement->bind_result($count);
            $statement->fetch();
            $statement->close();
            return $count;
        } else {
            // Should not be shown in production
            throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
        }
    }

    public function createOrUpdate(User $user): bool
    {
        // Check if user exists
        try {
            // Should probably have a function to check if user exists, but this triggers an exception I can use for now
            $this->getUserByUserID($user->id ?? -1);

            // Update existing user
            $statement = parent::$DB_CONN->prepare("UPDATE `{$this->tableName}` SET username = ?, password = ?, allowLogin = ?, isAdmin = ? WHERE id = ?");
            if ($statement) {
                $statement->bind_param('ssiii', $user->username, $user->password, $user->allowLogin, $user->isAdmin, $user->id);
                $statement->execute();
                if ($statement->error) {
                    $statement->close();
                    throw new DatabaseException($statement->error, $statement->errno);
                }
                $statement->close();
                return true;
            } else {
                // Should not be shown in production
                throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
            }
        } catch (NoResultException $e) {
            // Create new user
            $statement = parent::$DB_CONN->prepare("INSERT INTO `{$this->tableName}` (username, password, allowLogin, isAdmin) VALUES (?, ?, ?, ?)");
            if ($statement) {
                $statement->bind_param('ssii', $user->username, $user->password, $user->allowLogin, $user->isAdmin);
                $statement->execute();
                if ($statement->error) {
                    $statement->close();
                    throw new DatabaseException($statement->error, $statement->errno);
                }
                $statement->close();
                return true;
            } else {
                // Should not be shown in production
                throw new DatabaseException(parent::$DB_CONN->error, parent::$DB_CONN->errno);
            }
        }
    }
}
