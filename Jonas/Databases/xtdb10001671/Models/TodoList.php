<?php


namespace Jonas\Databases\xtdb10001671\Models;


use DateTime;

/**
 * Class TodoList
 * @package Jonas\Databases\xtdb10001671\Models
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class TodoList
{
    public ?int $id;
    public User $ownerUser;
    public string $name;
    public string $color;
    public ?DateTime $createTime;
    public array $items;

    /**
     * TodoList constructor.
     * @param int|null $id
     * @param User $ownerUser
     * @param string $name
     * @param string $color
     * @param DateTime|null $createTime
     * @param TodoListItem[] $items
     */
    public function __construct(?int $id, User $ownerUser, string $name, string $color, ?DateTime $createTime, array $items)
    {
        $this->id = $id;
        $this->ownerUser = $ownerUser;
        $this->name = $name;
        $this->color = $color;
        $this->createTime = $createTime;
        $this->items = $items;
    }

    /**
     * Checks if the given user owns the given list
     * @param TodoList $list
     * @param User $user
     * @return bool
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public static function checkIfUserOwns(TodoList $list, User $user): bool
    {
        return $list->ownerUser->id === $user->id;
    }
}
