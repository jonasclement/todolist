<?php


namespace Jonas\Databases\xtdb10001671\Models;


use DateTime;
use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\xtdb10001671\Tables\Users;

/**
 * Class User
 * @package Jonas\Databases\xtdb10001671\Models
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class User
{
    public ?int $id;
    public string $username;
    public string $password;
    public bool $allowLogin;
    public bool $isAdmin;
    public ?DateTime $createTime;

    public function __construct(?int $id, string $username, string $password, bool $allowLogin, bool $isAdmin, ?DateTime $createTime)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->allowLogin = $allowLogin;
        $this->isAdmin = $isAdmin;
        $this->createTime = $createTime;
    }

    /**
     * Check a user login attempt
     * @param $username
     * @param $password
     * @return bool
     * @throws DatabaseException|\Exception
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public static function checkLogin($username, $password): bool
    {
        $dbUsers = new Users();
        try {
            $user = $dbUsers->getUserByUsername($username);
            return password_verify($password, $user->password) && $user->allowLogin;
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
     * Check if the given username is taken
     * @param string $username
     * @return bool
     * @throws DatabaseException|\Exception
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public static function checkIfUsernameIsTaken(string $username): bool
    {
        $dbUsers = new Users();
        try {
            $user = $dbUsers->getUserByUsername($username);
            return true;
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
     * Checks if a username is valid
     * @param string $username
     * @return bool
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public static function validateUsername(string $username): bool
    {
        // Non-empty string containing A-Z, a-z, 0-9 and/or _ or -
        return !(preg_match('/[^a-z_\-0-9]/i', $username) === 1 && empty($username));
    }
}
