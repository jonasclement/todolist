<?php


namespace Jonas\Databases\xtdb10001671\Models;


use DateTime;
use Jonas\Databases\xtdb10001671\Tables\TodoLists;

/**
 * Class TodoListItem
 * @package Jonas\Databases\xtdb10001671\Models
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class TodoListItem
{
    public ?int $id;
    public int $todoListID;
    public string $text;
    public bool $done;
    public ?DateTime $createTime;

    public function __construct(?int $id, int $todoListID, string $text, bool $done, ?DateTime $createTime)
    {
        $this->id = $id;
        $this->todoListID = $todoListID;
        $this->text = $text;
        $this->done = $done;
        $this->createTime = $createTime;
    }

    /**
     * Get the associated to-do list
     * @return TodoList
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public function getTodoList(): TodoList
    {
        $dbTodoLists = new TodoLists();
        return $dbTodoLists->getListByID($this->todoListID);
    }
}
