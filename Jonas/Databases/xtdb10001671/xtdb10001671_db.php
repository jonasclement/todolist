<?php


namespace Jonas\Databases\xtdb10001671;

use Jonas\Databases\Database;
use Jonas\Databases\DatabaseConfiguration;
use JsonException;

/**
 * Class xtdb10001671_db
 * No, I did not name this database myself :-)
 * @package Jonas\Assignment\Database
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class xtdb10001671_db extends Database
{
    /**
     * @param string $tableName
     * @throws JsonException
     */
    protected function __construct(string $tableName)
    {
        $config = DatabaseConfiguration::fromConfigurationFile();
        parent::__construct($config->getHost(), $config->getUser(), $config->getPassword(), $config->getDatabase());
        $this->databaseName = 'xtdb10001671';
        $this->tableName = $tableName;
    }
}
