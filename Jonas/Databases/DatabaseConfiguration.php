<?php


namespace Jonas\Databases;


/**
 * Class DatabaseConfiguration
 * @package Jonas\Databases
 * @author Jonas Clément <contact@jonasclement.dk>
 */
class DatabaseConfiguration
{
    private string $host;
    private string $user;
    private string $password;
    private string $database;

    public function __construct(string $host, string $user, string $password, string $database)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
    }

    /**
     * Creates a new instance from configuration file
     * @param string $path
     * @return DatabaseConfiguration
     * @throws \JsonException
     * @author Jonas Clément <contact@jonasclement.dk>
     */
    public static function fromConfigurationFile(string $path = __DIR__ . '/../../config/database.json'): DatabaseConfiguration
    {

        $config = json_decode(
                file_get_contents(__DIR__ . '/../../config/database.json'),
                false,
                512,
                JSON_THROW_ON_ERROR
        );
        return new DatabaseConfiguration($config->host, $config->user, $config->password, $config->database);
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getDatabase(): string
    {
        return $this->database;
    }
}
