<?php
// Common code for all async helper files

// Development values - should be removed before entering production (or even better, be defined in a test-environment php.ini)
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/vendor/autoload.php';

session_start();

header('Content-Type: application/json');
