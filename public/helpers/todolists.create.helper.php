<?php

use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\xtdb10001671\Models\TodoList;
use Jonas\Databases\xtdb10001671\Tables\TodoLists;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => true, 'id' => -1, 'name' => '', 'color' => '', 'message' => ''];

$colors = [
        '#ff0000', // red
        '#800000', // maroon
        '#808000', // olive
        '#00FF00', // lime
        '#008000', // green
        '#008080', // teal
        '#800080' // purple
];

$dbTodoLists = new TodoLists();
$list = new TodoList(null, $_SESSION['user'], 'Min liste', $colors[array_rand($colors)], null, []);
try {
    $list = $dbTodoLists->create($list);
    $return = [
            'error' => false,
            'id' => $list->id,
            'name' => $list->name,
            'color' => $list->color,
            'message' => 'OK'
    ];
} catch (DatabaseException $e) {
    $return['message'] = $e->getMessage();
}

echo json_encode($return, JSON_THROW_ON_ERROR);
