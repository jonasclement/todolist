<?php

use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\xtdb10001671\Models\TodoListItem;
use Jonas\Databases\xtdb10001671\Tables\TodoListItems;
use Jonas\Databases\xtdb10001671\Tables\TodoLists;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => true, 'id' => -1, 'message' => ''];

$todoListID = $_POST['todoListID'] ?? -1;
$text = $_POST['text'] ?? '';

if (empty($text)) {
    $return['message'] = 'Text is empty';
} else {
    try {
        $dbTodoLists = new TodoLists();
        $list = $dbTodoLists->getListByID((int)$todoListID);
        if (!$list->ownerUser->id === $_SESSION['user']->id) {
            throw new NoResultException();
        }

        $dbTodoListItems = new TodoListItems();
        $item = new TodoListItem(null, $list->id, $text, false, null);
        $item = $dbTodoListItems->create($item);
        $return = [
                'error' => false,
                'id' => $item->id,
                'message' => 'OK'
        ];
    } catch (NoResultException $e) {
        $return['message'] = "List {$todoListID} does not exist";
    } catch (DatabaseException $e) {
        // Should not be shown in production
        $return['message'] = $e->getMessage();
    }
}

echo json_encode($return, JSON_THROW_ON_ERROR);
