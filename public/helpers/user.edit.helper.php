<?php

use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\xtdb10001671\Models\User;
use Jonas\Databases\xtdb10001671\Tables\Users;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => false, 'message' => 'OK'];

// Gather inputs
$userID = (int)$_POST['userID'];
$username = $_POST['username'] ?? '';
$password = $_POST['password'] ?? '';
$allowLogin = (bool)$_POST['allowLogin'];
$isAdmin = (bool)$_POST['isAdmin'];

$dbUsers = new Users();

// Check if user is allowed to make this change
if ($_SESSION['user']->id !== $userID && !$_SESSION['user']->isAdmin) {
    $return['error'] = true;
    // In production, this should be changed to match the user not found message to avoid exposing valid user IDs
    $return['message'] = "Bruger {$_SESSION['user']->id} må ikke ændre bruger {$userID}";
}

// Check if username is valid
if (!User::validateUsername($username)) {
    $return['error'] = true;
    $return['message'] = 'Ugyldigt brugernavn';
}

try {
    $user = $dbUsers->getUserByUserID($userID);
} catch (NoResultException $e) {
    $return['error'] = true;
    $return['message'] = "Bruger med ID {$userID} eksisterer ikke";
}

// Check if username is taken
if ($username !== $user->username && User::checkIfUsernameIsTaken($username)) {
    $return['error'] = true;
    $return['message'] = "Brugernavn {$username} er allerede i brug";
}

// If no errors at this point, go ahead
if (!$return['error']) {
    // Update user
    $user->username = $username;
    $user->allowLogin = $allowLogin;
    $user->password = empty($password) ? $user->password : password_hash($password, PASSWORD_DEFAULT);
    if ($user->isAdmin && $isAdmin === false) {
        // There must always be at least one admin
        if ($dbUsers->getAmountOfAdminUsers() === 1) {
            $isAdmin = true;
        }
    }

    if ($user->isAdmin) {
        $user->isAdmin = $isAdmin;
    }

    try {
        $dbUsers->createOrUpdate($user);
    } catch (DatabaseException $e) {
        $return['error'] = true;
        $return['message'] = "Der skete en fejl i databasen: {$e->getMessage()}";
    }
}

echo json_encode($return, JSON_THROW_ON_ERROR);
