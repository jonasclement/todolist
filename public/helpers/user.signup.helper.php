<?php

use Jonas\Databases\xtdb10001671\Models\User;
use Jonas\Databases\xtdb10001671\Tables\Users;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => true];

$username = $_POST['username'];
$password = $_POST['password'];

if (User::validateUsername($username) && !User::checkIfUsernameIsTaken($username)) {

    // Create user and log in
    $dbUsers = new Users();
    $user = new User(null, $username, password_hash($password, PASSWORD_DEFAULT), true, false, null);
    if ($dbUsers->createOrUpdate($user)) {
        $return['error'] = false;
        $_SESSION['user'] = $dbUsers->getUserByUsername($username);
    }
}

echo json_encode($return, JSON_THROW_ON_ERROR);
