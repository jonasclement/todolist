<?php

use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\xtdb10001671\Models\TodoList;
use Jonas\Databases\xtdb10001671\Tables\TodoListItems;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => true, 'message' => ''];

$id = $_POST['id'] ?? -1;
$text = $_POST['text'] ?? '';
$done = $_POST['done'] ?? '';
$return['done'] = $done;
$done = $done === 'true';

$dbTodoListItems = new TodoListItems();
try {
    $item = $dbTodoListItems->getById((int)$id);
    $list = $item->getTodoList();
    if (TodoList::checkIfUserOwns($list, $_SESSION['user'])) {
        $dbTodoListItems->updateItemByID((int)$id, $text, $done);
        $return['error'] = false;
        $return['message'] = 'OK';
    } else {
        $return['error'] = true;
        $return['message'] = "User does not have permission to change this item";
    }
} catch (NoResultException $e) {
    $return['error'] = true;
    $return['message'] = "User does not have permission to change this item";
} catch (DatabaseException $e) {
    $return['message'] = $e->getMessage();
}

echo json_encode($return, JSON_THROW_ON_ERROR);
