<?php

use Jonas\Databases\Exceptions\DatabaseException;
use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\xtdb10001671\Models\TodoList;
use Jonas\Databases\xtdb10001671\Tables\TodoLists;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => true, 'message' => ''];

$id = (int)$_POST['id'];
$name = $_POST['name'] ?? '';
$color = $_POST['color'] ?? '';

$dbTodoLists = new TodoLists();

try {
    $list = $dbTodoLists->getListByID($id);
    if (TodoList::checkIfUserOwns($list, $_SESSION['user'])) {
        $list->name = !empty($name) ? $name : $list->name;
        $list->color = !empty($color) ? $color : $list->color;
        $dbTodoLists->update($list);
        $return['error'] = false;
        $return['message'] = 'OK';
    } else {
        $return['error'] = true;
        $return['message'] = 'User does not have permission to change this list';
    }
} catch (NoResultException $e) {
    $return['error'] = true;
    $return['message'] = "User does not have permission to change this list";
} catch (DatabaseException $e) {
    // Should not be shown in production
    $return['message'] = $e->getMessage();
}

echo json_encode($return, JSON_THROW_ON_ERROR);
