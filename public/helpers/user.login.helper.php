<?php

use Jonas\Databases\xtdb10001671\Models\User;
use Jonas\Databases\xtdb10001671\Tables\Users;

require_once __DIR__ . '/../../helperBoilerplate.php';

$return = ['error' => false, 'allowLogin' => false];

$loginCheck = User::checkLogin($_POST['username'], $_POST['password']);
if ($loginCheck) {
    $dbUsers = new Users();
    $_SESSION['user'] = $dbUsers->getUserByUsername($_POST['username']);
    $return = ['error' => false, 'allowLogin' => true];
} else {
    // To make this helper (more) timing constant - thereby making it harder to brute-force guess valid usernames - verify
    // a given hash with password_verify() before returning
    password_verify('bogus', '$2y$10$prYqdkeuIOCPxf82NhWQQ../RMhxZcpzE07GvcRhgg9mpDIbA.Uru');
    http_response_code(401);
}

echo json_encode($return);
