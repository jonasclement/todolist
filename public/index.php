<?php

use Jonas\Databases\Exceptions\NoResultException;
use Jonas\Databases\xtdb10001671\Tables\TodoLists;
use Jonas\Databases\xtdb10001671\Tables\Users;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// Development values - should be removed before entering production (or even better, be defined in a test-environment php.ini)
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';

session_start();

// Check user is still valid
$dbUsers = new Users();
if (isset($_SESSION['user'])) {
    try {
        $user = $dbUsers->getUserByUserID($_SESSION['user']->id);
        if (!$user->allowLogin) {
            session_destroy();
        }
    } catch (NoResultException $e) {
        session_destroy();
    }
}

$request = $_GET['request'] ?? 'index.php';

$loginNotRequiredRequests = ['login.php', '404.php'];
if (!isset($_SESSION['user']) && !in_array($request, $loginNotRequiredRequests, true)) {
    $request = 'login.php';
}

$loader = new FilesystemLoader(__DIR__ . '/templates');
$twig = new Environment($loader);

switch ($request) {
    case 'login.php':
        if (!isset($_SESSION['user'])) {
            $template = 'login.html.twig';
        } else {
            header('Location: index.php');
        }
        break;
    case 'logout.php':
        session_destroy();
        header('Location: login.php');
        break;
    case 'index.php':
        $dbTodoLists = new TodoLists();
        $variables = ['lists' => $dbTodoLists->getAllListsWithItemsForWebByUser($_SESSION['user'])];
        $template = 'index.html.twig';
        break;
    case 'user-management.php':
        if ($_SESSION['user']->isAdmin) {
            // Get users
            $variables = ['users' => $dbUsers->getAllUsers()];
            $template = 'user-management.html.twig';
        } else {
            header('Location: index.php');
        }
        break;
    case '404.php':
    default:
        $template = '404.html.twig';
        $variables = ['page' => $request];
        break;
}

// Add variables that should be present for every page
$variables = array_merge(
        $variables ?? [],
        [
                'currentyear' => date('yy'),
                'user' => $_SESSION['user'] ?? null
        ]
);

try {
    echo $twig->render($template, $variables);
} catch (\Twig\Error\LoaderError $e) {
    var_dump($e);
} catch (\Twig\Error\RuntimeError $e) {
    var_dump($e);
} catch (\Twig\Error\SyntaxError $e) {
    var_dump($e);
}
